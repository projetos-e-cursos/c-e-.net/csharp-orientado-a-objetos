﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByteBank.Models
{
    class SaldoInsulficienteException : Exception
    {
        public double Saldo { get; }
        public double ValorSaque{ get; }
        public SaldoInsulficienteException()
        {

        }
        public SaldoInsulficienteException(string msg) : base(msg)
        {

        }
        public SaldoInsulficienteException(string msg, double saldo, double valorSaque) : this(msg)
        {
            Saldo = saldo;
            ValorSaque = valorSaque;
        }
    }
}
