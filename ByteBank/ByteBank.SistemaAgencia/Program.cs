﻿using ByteBank.Models;
using ByteBank.Models.Funcionarios;
using ByteBank.SistemaAgencia.Extensoes;
using Humanizer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ByteBank.SistemaAgencia
{
    class Program
    {
        static void Main(string[] args)
        {
            var idades = new List<int>();
            idades.Add(9);
            idades.Add(19);
            idades.Add(29);
            idades.Remove(19);

            idades.AdicionarVarios(19, 39, 49, 59);
            idades.AdicionarVarios(99, 0);

            idades.Sort();

            Console.WriteLine(idades);

            var nomes = new List<string>()
            {
                "Guilherme",
                "Luana",
                "Ana"
            };
            
            nomes.Add("Erick");
            nomes.AdicionarVarios("Jorge", "Zabix");

            nomes.Sort();

            foreach (var nome in nomes)
            {
                Console.WriteLine(nome);
            }

            var contas = new List<ContaCorrente>();

            contas.Add(new ContaCorrente(865, 123456));
            contas.Add(new ContaCorrente(869, 123457));
            contas.Add(new ContaCorrente(868, 123458));
            contas.Add(new ContaCorrente(867, 123459));

            var contasOrdenadas = contas
                .Where(conta => conta != null)
                .OrderBy(conta => conta.Numero);

            foreach (var conta in contas)
            {
                Console.WriteLine($"Conta número {conta.Numero}, ag. {conta.Agencia}");
            }

            Console.ReadLine();
        }

        static void TestaMetodoDeExtensao()
        {
            List<int> idades = new List<int>();
            idades.Add(9);
            idades.Add(19);
            idades.Add(29);
            idades.Remove(19);

            idades.AdicionarVarios(19, 39, 49, 59);

            Console.WriteLine(idades.Count);
        }
        
        static void TestaListaCriada()
        {
            Lista<int> idades = new Lista<int>();

            idades.Adicionar(8);
            idades.Adicionar(18);
            idades.Adicionar(28);
            idades.Adicionar(38);
            idades.Adicionar(48);

            int totalIdades = 0;
            for (int i = 0; i < idades.Tamanho; i++)
            {
                totalIdades += idades[i];
            }
            Console.WriteLine(totalIdades);
        }

        static void TestaListaDeObjetct()
        {
            ListaDeObject listaDeIdades = new ListaDeObject();

            listaDeIdades.Adicionar(10);
            listaDeIdades.Adicionar(20);
            listaDeIdades.Adicionar(5);
            listaDeIdades.AdicionarVarios(16, 23, 60);

            for (int i = 0; i < listaDeIdades.Tamanho; i++)
            {
                int idade = (int)listaDeIdades[i];
                Console.WriteLine($"Idade no indice {i} igual a {idade}");
            }
        }

        static void TestaListaDeContaCorrente()
        {
            ListaDeContaCorrente lista = new ListaDeContaCorrente(6);

            lista.Adicionar(new ContaCorrente(874, 5679787));
            lista.Adicionar(new ContaCorrente(874, 5679788));
            lista.Adicionar(new ContaCorrente(874, 5679789));
            lista.Adicionar(new ContaCorrente(874, 5679790));
            lista.Adicionar(new ContaCorrente(874, 5679791));
            lista.Adicionar(new ContaCorrente(874, 5679792));

            for (int i = 0; i < lista.Tamanho; i++)
            {
                ContaCorrente itemAtual = lista.GetItemNoIndice(i);
                Console.WriteLine($"Item na posição {i} = Conta {itemAtual.Numero}/{itemAtual.Agencia}");

            }

            lista.AdicionarVarios(
                new ContaCorrente(874, 5679787),
                new ContaCorrente(874, 5679788)
            );
        }

        static void TestaArreyDeContaCorrente()
        {
            ContaCorrente[] contas = new ContaCorrente[]
            {
                new ContaCorrente(874, 5679787),
                new ContaCorrente(874, 5679788),
                new ContaCorrente(874, 5679789)
            };

            foreach (var conta in contas)
            {
                Console.WriteLine($"Número da conta: {conta}");
            }
        }

        static void TestaArrayInt()
        {
            // ARRAY de inteiros, com 5 posições!
            int[] idades = new int[5];
            idades[0] = 15;
            idades[1] = 28;
            idades[2] = 35;
            idades[3] = 50;
            idades[4] = 28;

            Console.WriteLine(idades[4]);

            int idadeNoIndice4 = idades[4];

            Console.WriteLine(idadeNoIndice4);

            int acumulador = 0;
            foreach (var idade in idades)
            {
                acumulador += idade;
            }

            Console.WriteLine(acumulador);
        }
    }
}
