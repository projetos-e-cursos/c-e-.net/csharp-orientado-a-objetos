﻿using ByteBank.Funcionarios;
using ByteBank.Sistemas;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByteBank
{
    class Program
    {
        static void Main(string[] args)
        {
            CarregarContas();


            //CalcularBonificacao();
            //UsarSistema(); Métodos dos cursos de Orientação a Objetos
            //Console.WriteLine(ContaCorrente.TaxaOperacao);
            try
            {
                //Metodo();
                //Console.WriteLine("Try da chamada do Metodo()");
            }
            catch (Exception)
            {
                Console.WriteLine("Exceção não esperada");
            }

            Console.WriteLine("Execução finalizada... Tecle enter para sair. . .");
            Console.ReadLine();
        }

        private static void CarregarContas()
        {

            using(LeitorDeArquivo leitor = new LeitorDeArquivo("teste.txt"))
            {
                leitor.LerProximaLinha();
                leitor.LerProximaLinha();
                leitor.LerProximaLinha();
            }

            // ---------------------------------

            //LeitorDeArquivo leitor = null;
            //try
            //{
            //    leitor = new LeitorDeArquivo("contas.txt");

            //    leitor.LerProximaLinha();
            //    leitor.LerProximaLinha();
            //    leitor.LerProximaLinha();

            //    leitor.Fechar();
            //}
            //catch (IOException)
            //{
            //    Console.WriteLine("Exceção do tipo IOException capturada e tratada!");
            //}
            //finally
            //{
            //    if(leitor != null){
            //        leitor.Fechar();
            //    }
            //}
        }

        private static void TestaInnerException()
        {
            try
            {
                ContaCorrente conta = new ContaCorrente(874, 874150);
                ContaCorrente conta2 = new ContaCorrente(874, 874150);
                conta.Depositar(50);
                conta.Sacar(500);
                conta.Transferir(-10, conta2);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine("Ocorreu uma exceção do tipo ArgumentException");
                Console.WriteLine(ex.Message);
            }
            catch (SaldoInsulficienteException ex)
            {
                Console.WriteLine("Ocorreu uma exceção do tipo SaldoInsulficienteException");
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.Saldo);
                Console.WriteLine(ex.ValorSaque);
            }
        }

        // Funções de tratamento de exceções
        private static void Metodo()
        {
            TestaDivisao(2);
        }

        private static void TestaDivisao(int divisor)
        {
            try
            {
                int resultado = Dividir(10, divisor);

                Console.WriteLine("Resultado da divisão de 10 por " + divisor + " é " + resultado);
            }
            catch (DivideByZeroException erro)
            {
                Console.WriteLine(erro.Message);
                Console.WriteLine("Não é possível fazer uma divisão por 0!");
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("Não é possível acessar atributo de uma referencia nula!");
            }
        }
        
        private static int Dividir(int numero, int divisor)
        {
            ContaCorrente conta = null;
            Console.WriteLine(conta.Saldo);
            return numero / divisor;
        }

        // Funções dos cursos de Orientação a Objetos
        public static void UsarSistema()
        {
            SistemaInterno sistemaInterno = new SistemaInterno();

            Diretor roberta = new Diretor("159.753.398-04");
            roberta.Nome = "Roberta";
            roberta.Senha = "123";

            sistemaInterno.Logar(roberta, "123");
            sistemaInterno.Logar(roberta, "abc");

            GerenteDeConta camila = new GerenteDeConta("326.9855.628-89");
            camila.Nome = "Camila";
            camila.Senha = "abc";
            sistemaInterno.Logar(camila, "123");
            sistemaInterno.Logar(camila, "abc");

            ParceiroComercial parceiro = new ParceiroComercial();
            parceiro.Senha = "123456";
            sistemaInterno.Logar(parceiro, "123456");
        }

        public static void CalcularBonificacao()
        {
            GerenciadorBonificacao gerenciadorBonificacao = new GerenciadorBonificacao();

            Designer pedro = new Designer("833.222.048-39");
            pedro.Nome = "Pedro";

            Diretor roberta = new Diretor("159.753.398-04");
            roberta.Nome = "Roberta";


            Auxiliar igor = new Auxiliar("981.1983778-53");
            igor.Nome = "Igor";

            GerenteDeConta camila = new GerenteDeConta("326.9855.628-89");
            camila.Nome = "Camila";

            gerenciadorBonificacao.Registrar(pedro);
            gerenciadorBonificacao.Registrar(roberta);
            gerenciadorBonificacao.Registrar(igor);
            gerenciadorBonificacao.Registrar(camila);

            Console.WriteLine("Total de bonificações do mês " +
                gerenciadorBonificacao.GetTotalBonificacao());

        }
    }
}
