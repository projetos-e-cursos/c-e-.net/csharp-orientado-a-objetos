﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByteBank.Models.Funcionarios
{
    public class Desenvolvedor : Funcionario
    {
        public Desenvolvedor(string cpf) : base(5000, cpf)
        {
            Console.WriteLine("Criando um novo Desenvolvedor");
        }

        public override void AumentarSalario()
        {
            throw new NotImplementedException();
        }

        internal protected override double GetBonificacao()
        {
            throw new NotImplementedException();
        }
    }
}
