﻿using System;
using System.Diagnostics.Eventing.Reader;

namespace ByteBank.Models
{
    /// <summary>
    /// Define uma Conta Corrente do banco ByteBank.
    /// </summary>
    public class ContaCorrente : IComparable
    {
        private double _saldo = 100;
        private double _limite = 100;
        public static double TaxaOperacao { get; private set; }
        public static int TotalDeContasCriadas { get; private set; }
        public Cliente Titular { get; set; }
        public int Agencia { get; }
        public int Numero { get; }
        public int ContadorSaquesNaoPermitidos { get; private set; }
        public int ContadorTransferenciasNaoPermitidas { get; private set; }

        /// <summary>
        /// Cria uma uma instância de ContaCorrente com os argumentos utilizados.
        /// </summary>
        /// <param name="agencia">Representa o valor da propriedade <see cref="Agencia"/> Agencia e deve possuir um valor maior que zero</param>
        /// <param name="numero">Representa o valor da propriedade <see cref="Numero"/> e deve possuir um valor maior que zero</param>
        public ContaCorrente(int agencia, int numero)
        {
            try
            {
                if (agencia <= 0)
                {
                    throw new ArgumentException("Agencia deve ser maior que 0.", nameof(agencia));
                }
                if (numero <= 0)
                {
                    throw new ArgumentException("Numero deve ser maior que 0.", "numero");
                }
                Agencia = agencia;
                Numero = numero;

                TotalDeContasCriadas++;
                TaxaOperacao = 30 / TotalDeContasCriadas;
            }
            catch (DivideByZeroException e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public double Saldo
        {
            get
            {
                return _saldo;
            }
            set
            {
                if (_saldo < 0)
                {
                    return;
                }
                _saldo = value;
            }
        }

        /// <summary>
        /// Realiza o saque e atualiza o valor da propriedade <see cref="Saldo"/>
        /// </summary>
        /// <exception cref="ArgumentException"> Exceção lançada quando um valor negativo é utilizado no argumento <paramref name="valor"/></exception>
        /// <exception cref="SaldoInsulficienteException"> Exceção lançada quando o valor de <paramref name="valor"/> é maior que o valor da propriedade <see cref="Saldo"/></exception>
        /// <param name="valor">Representa o valor requisitado para o saque. Deve ser maior que zero e menos que o <see cref="Saldo"/>.</param>
        public void Sacar(double valor)
        {
            if (valor < 0)
            {
                throw new ArgumentException("Valor inválido para o saque.", nameof(valor));
            }
            if (_saldo + _limite < valor)
            {
                ContadorSaquesNaoPermitidos++;
                throw new SaldoInsulficienteException("Valor requisitado é maior que o valor permitido para saque!", _saldo, valor);
            }
            else
            {
                _saldo -= valor;
            }
        }

        public void Depositar(double valor)
        {
            _saldo += valor;
        }

        public void Transferir(double valor, ContaCorrente contaDestino)
        {
            if (valor < 0)
            {
                throw new ArgumentException("Valor inválido para transferência.", nameof(valor));
            }
            try
            {
                Sacar(valor);
            }
            catch (SaldoInsulficienteException ex)
            {
                ContadorTransferenciasNaoPermitidas++;
                throw new OperacaoFinanceiraException("Operação não realizada.", ex);
            }
            
            contaDestino.Depositar(valor);
        }

        public override string ToString()
        {
            return $"Número {Numero}, Agência {Agencia}, Saldo {Saldo}";
        }

        // override object.Equals
        public override bool Equals(object obj)
        {
            ContaCorrente outraConta = obj as ContaCorrente;
            if (obj == null)
            {
                return false;
            }

            return Numero == outraConta.Numero && Agencia == outraConta.Agencia;
        }

        public int CompareTo(object obj)
        {
            // Retornar negativo quando a instância precede o obj
            // Retornar zero quando a instância e obj forem equivalentes
            // Retornar positivo quando o obj precede a instância

            var outraConta = obj as ContaCorrente;

            if (outraConta == null || Numero < outraConta.Numero)
            {
                return -1;
            }
            else if (Numero == outraConta.Numero)
            {
                return 0;
            }

            return 1;

        }
    }
}