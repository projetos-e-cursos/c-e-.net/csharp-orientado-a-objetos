﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_ByteBank
{
    class Program
    {
        static void Main(string[] args)
        {
            ContaCorrente contaDoBruno = new ContaCorrente();

            contaDoBruno.titular = "Bruno";
            contaDoBruno.saldo = 150;

            bool resultadoSaque = contaDoBruno.Sacar(50);

            Console.WriteLine(contaDoBruno.saldo);

            ContaCorrente contaDaGabriela = new ContaCorrente();

            Console.WriteLine(contaDoBruno.saldo);
            Console.WriteLine(contaDaGabriela.saldo);

            contaDoBruno.Transferir(50, contaDaGabriela);

            Console.WriteLine(contaDoBruno.saldo);
            Console.WriteLine(contaDaGabriela.saldo);
            

            Console.ReadLine();
        }
    }
}
