﻿using System;
using System.Diagnostics.Eventing.Reader;

namespace ByteBank
{
    public class ContaCorrente
    {
        private double _saldo = 100;
        private double _limite = 100;
        public static double TaxaOperacao { get; private set; }
        public static int TotalDeContasCriadas { get; private set; }
        public Cliente Titular { get; set; }
        public int Agencia { get; }
        public int Numero { get; }
        public int ContadorSaquesNaoPermitidos { get; private set; }
        public int ContadorTransferenciasNaoPermitidas { get; private set; }

        public ContaCorrente(int agencia, int numero)
        {
            try
            {
                if (agencia <= 0)
                {
                    throw new ArgumentException("Agencia deve ser maior que 0.", nameof(agencia));
                }
                if (numero <= 0)
                {
                    throw new ArgumentException("Numero deve ser maior que 0.", "numero");
                }
                Agencia = agencia;
                Numero = numero;

                TotalDeContasCriadas++;
                TaxaOperacao = 30 / TotalDeContasCriadas;
            }
            catch (DivideByZeroException e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public double Saldo
        {
            get
            {
                return _saldo;
            }
            set
            {
                if (_saldo < 0)
                {
                    return;
                }
                _saldo = value;
            }
        }

        public void Sacar(double valor)
        {
            if (valor < 0)
            {
                throw new ArgumentException("Valor inválido para o saque.", nameof(valor));
            }
            if (_saldo + _limite < valor)
            {
                ContadorSaquesNaoPermitidos++;
                throw new SaldoInsulficienteException("Valor requisitado é maior que o valor permitido para saque!", _saldo, valor);
            }
            else
            {
                _saldo -= valor;
            }
        }

        public void Depositar(double valor)
        {
            _saldo += valor;
        }

        public void Transferir(double valor, ContaCorrente contaDestino)
        {
            if (valor < 0)
            {
                throw new ArgumentException("Valor inválido para transferência.", nameof(valor));
            }
            try
            {
                Sacar(valor);
            }
            catch (SaldoInsulficienteException ex)
            {
                ContadorTransferenciasNaoPermitidas++;
                throw new OperacaoFinanceiraException("Operação não realizada.", ex);
            }
            
            contaDestino.Depositar(valor);
        }
    }
}